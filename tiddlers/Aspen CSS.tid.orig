created: 20151215203802229
modified: 20160209161719543
tags: Aspen CSS
title: Aspen CSS
type: text/css

<style>
html,
body {
	margin:0;
	padding:0;
	height:100%;
}
body.aspen
{
	font-family: 'PT Sans', sans-serif;	
	font-size: 18px;
	font-weight:400;
	color:#343434;
}
.hide
{
	display:none !important;
    height:0 !important;
}
.not-active {
   pointer-events: none;
   cursor: default;
}
.coming-soon
{
	opacity: .8;
	background: rgba(0, 0, 0, 0.39); 
	z-index: 1000;
    width:100%;
    height:50%;
    padding: 2% 0;
}
.coming-soon h1
{
	font-style: italic;
    font-family: 'PT Sans', sans-serif;	
}
.coming-soon .nav-center
{
	opacity: .5;
}
.showSingle
{
	
}
.showSingle.active
{
	color: #ffffff;
	background-color:#61b2b9;
}
sup
{
	font-size: .6em;
}
.clearfix::after
{
	content: '';
	display: table;
	clear: both;
}

.aspen p
{
    margin:0;
    padding:0;	
    text-align: justify;
}
.aspen section p
{
    margin:1em 0;
    text-align: justify;
}
.aspen p img.fullwidth
{
	display:table;
}
.aspen section p {
    -webkit-margin-before: 1em;
  -webkit-margin-after: 1em;
  -webkit-margin-start: 0px;
  -webkit-margin-end: 0px;
}
.aspen section ul 
{
    list-style: circle;
    margin-left: 1.2em;
}
h1
{
	font-family: 'Open Sans', sans-serif;
	text-transform:uppercase;	
	color:#343434;
	font-size: 2em;
	font-weight: 300;
	/*border-bottom: dashed 1px #015c80;*/
    text-align:center;

}
.twelve h1
{
	display:block;
    
}


h2
{
	font-family: 'Open Sans', sans-serif;
	text-transform:uppercase;	
	color:#343434;
	font-size: 1.6em;
	font-weight: 300;
	/*border-bottom: dashed 1px #015c80;*/
	text-align:center;
}
h3
{
	font-family: 'Open Sans', sans-serif;
	text-transform:uppercase;	
	color:#343434;
	font-size: 1.3em;
	font-weight: 300;
    margin-bottom:0;
	text-align:center;
}
h4
{
	font-family: 'Open Sans', sans-serif;
	text-transform:uppercase;	
	color:#343434;
	font-size: 1.1em;
	font-weight: 300;
	/*border-bottom: dashed 1px #015c80;*/
	text-align:center;
}
#div1 h4
{
	margin-bottom: 0;
	font-weight:500;
    text-align:left;
}
.text_center, .aspen section .text_center p
{
	text-align:center;
}
#div1 p
{
	margin-top:0;
	text-align:left;
}
#div1 h4 + p
{
	font-style: italic;
}
section
{
	margin: 20% 0;
}
section.grey
{
	background: #e4e5e7;
    margin: 0;
    padding: 20% 0;
}
section.career
{
	padding-bottom: 0;
    margin-bottom: 0;
}
section.career iframe
{
	margin-bottom: 10em;
}	
section.extra_pad
{
	padding: 10% 0;
}
section a 
{
	color: #f78e1d;
	text-decoration:underline;
}

.button-or-solid
{
	font-family: 'Open Sans', sans-serif;
	color: #ffffff;
	background-color:#f78e1d;	
	text-transform:none;
	font-size:1.5rem;
	font-weight:300;
	height:2.7em;
    width:100%;
}
.button-tl-solid
{
	font-family: 'Open Sans', sans-serif;
	color: #ffffff;
	background-color:#61b2b9;	
	text-transform:none;
	font-size:1.5rem;
	font-weight:300;
	height:2.7em;
}
a.button-or-solid:hover, a.button-tl-solid:hover
{
	color: #ffffff;
}
/* Trim */
.hvr-trim {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: translateZ(0);
  transform: translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  -moz-osx-font-smoothing: grayscale;
  position: relative;
}
.hvr-trim:before {
  content: '';
  position: absolute;
  border: white solid 2px;
  top: 2px;
  left: 2px;
  right: 2px;
  bottom: 2px;
  opacity: 0;
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
  -webkit-transition-property: opacity;
  transition-property: opacity;
}
.hvr-trim:hover:before, .hvr-trim:focus:before, .hvr-trim:active:before {
  opacity: 1;
  color: #ffffff;
}



.button-or-outline
{
	font-family: 'Open Sans', sans-serif;
	color: #ff8a0c;
	border-color:#ff8a0c;	
	text-transform:none;
	font-size:1.5rem;
	font-weight:400;
	height:2.7em;
}

.one-third .button-or-outline
{
	width:100%;
}
/*----Parallax setup----------*/



section.module.content {
  padding: 20% 0;
}
section.module.parallax {
  height: 500px;
  margin:0;
  background-position: 50% 50%;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}
section.module.parallax h1 {
  color: rgba(255, 255, 255, 0.8);
  font-size: 3em;
  line-height: 9em;
  font-weight: 700;
  text-align: center;
  display:block;
  font-family: 'Open Sans', sans-serif;
  text-transform:uppercase;	
  text-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
}
section.module.parallax-forest {
  background-color: #f5dc6b;
  background-image: url(../images/aspen/forest.jpg);
}
section.module.parallax-forest h1
{
	font-size: 3em;
    line-height: 1.3em;
    padding: 20% 0;
}
section.module.parallax-asfg {
  background-image: url(../images/aspen/atlanta_city.jpg);
}







/* Shutter Out Horizontal */
.hvr-shutter-out-horizontal {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: translateZ(0);
  transform: translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  -moz-osx-font-smoothing: grayscale;
  position: relative;
  -webkit-transition-property: color;
  transition-property: color;
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;

}
.hvr-shutter-out-horizontal:before {
  content: "";
  position: absolute;
  z-index: -1;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, .2);
  -webkit-transform: scaleX(0);
  transform: scaleX(0);
  -webkit-transform-origin: 50%;
  transform-origin: 50%;
  -webkit-transition-property: transform;
  transition-property: transform;
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
  -webkit-transition-timing-function: ease-out;
  transition-timing-function: ease-out;
}
.hvr-shutter-out-horizontal:hover, .hvr-shutter-out-horizontal:focus, .hvr-shutter-out-horizontal:active {
  color: black;
}
.hvr-shutter-out-horizontal:hover:before, .hvr-shutter-out-horizontal:focus:before, .hvr-shutter-out-horizontal:active:before {
  -webkit-transform: scaleX(1);
  transform: scaleX(1);
}



/*-------- Navbar  START --------*/

nav
{
	
	font-weight:300;
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#adfce7+0,61b2b9+77 */
	background: #adfce7; /* Old browsers */
	background: -moz-linear-gradient(-45deg,  #adfce7 0%, #61b2b9 77%); /* FF3.6-15 */
	background: -webkit-linear-gradient(-45deg,  #adfce7 0%,#61b2b9 77%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(135deg,  #adfce7 0%,#61b2b9 77%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#adfce7', endColorstr='#61b2b9',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}
nav h1, nav h2, nav p
{
	text-align:center;
	color: #ffffff;	
}
nav h1
{
	font-family: 'Open Sans', sans-serif;
	font-weight: 300;
	font-size:3em;
	border-bottom: none;
	display: block;
}
.claims_section h1
{
	font-size: 2.5em;
}
nav h2
{
	font-family: 'Open Sans', sans-serif;
	font-weight: 300;
	font-size:2em;
	border-bottom: none;
	display: block;
}
.aspen nav p
{
	font-family: 'PT Sans', sans-serif;	
	font-size: 1em;
	font-weight:400;	
    text-align: center;
}
.nav-center, .aspen section p.nav-center
{
	text-align:center;
}

.nav-bg
{
	text-align:center;	
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+50,ffffff+99&0+1,1+50,0+100 */
	background: -moz-linear-gradient(left,  rgba(255,255,255,0) 1%, rgba(255,255,255,1) 50%, rgba(255,255,255,0.02) 99%, rgba(255,255,255,0) 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(left,  rgba(255,255,255,0) 1%,rgba(255,255,255,1) 50%,rgba(255,255,255,0.02) 99%,rgba(255,255,255,0) 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to right,  rgba(255,255,255,0) 1%,rgba(255,255,255,1) 50%,rgba(255,255,255,0.02) 99%,rgba(255,255,255,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#00ffffff',GradientType=1 ); /* IE6-9 */
}

.nav-container
{
	padding-bottom: 5%;	
}

.logo-wrap
{
	z-index: 100;
    position: relative;
}
.logo-wrap a
{
	display: inline-block;	
}
.logo-wrap a img
{
	width: 80%;	
	margin:8% 0 5% 0;
}
.logo-wrap a:hover
{
	background-color: transparent;
}






.nav-wrap
{
	z-index: 1000;
    position: relative;
	background: rgba(0, 0, 0, 0.39); 	
	width:100%;
	margin:0;
}
.nav-wrap .container
{
	width: 90%;
}
#showmenu
{
	width: 15%;	
	float: right;
    cursor: pointer;
	margin: 2% 0 0 0;
}

#showmenu img
{
	width: 100%;
	max-width: 30px;	
}
#div4 table
{
	max-width: 390px;
    margin: 0 auto;
}

.social_icons
{
	height: 20px;
}
footer .social_icons
{
height: auto;
	width: 30px;
}
.contact_email
{

}

/*-------- Navbar  END --------*/




/*-------- Footer START --------*/
footer 
{
	display: block;
	width: 100%;
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#adfce6+0,61b2b9+100 */
	background: #adfce6; /* Old browsers */
	background: -moz-linear-gradient(top,  #adfce6 0%, #61b2b9 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top,  #adfce6 0%,#61b2b9 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom,  #adfce6 0%,#61b2b9 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#adfce6', endColorstr='#61b2b9',GradientType=0 ); /* IE6-9 */
	z-index: 99;
	text-align:center;
	font-family: 'Open Sans', sans-serif;
	font-weight: 300;
	line-height: 3rem;
	color: #fff;
	font-size:.8em;
    width:100%;

 }
footer > .container 
{
	width: 100%; 
}

footer img
{
	max-width:200px;
	width: 80%;	
	/*margin:2em 0 0 0;*/
    margin:0;
}

footer p.footer_links img
{
margin: 0 1em 0 0;
	display: inline-block;
}
footer p.footer_links
{
	text-align:center;
    vetical-align: top;
    margin-top: 1em;
}
footer p.footer_links a
{
	margin-right:1em;
}
.footer-link 
{
	text-decoration: underline;
	color: #fff; 
    transition: all 0.3s ease;
}
.footer-link.active 
{
	color: #33C3F0; 
}
.footer-link:hover 
{
	font-weight: 600;
    color: #ffffff;
}
.footer_memberlogos
{
	width: auto;
    vertical-align:top;
    height:40px;
    margin:0;
}
.footer_asfglink
{
	display:block;
}
.footer_asfglogo
{
	width: 230px;
    vertical-align:top;
    margin:0;
    
}
footer h3
{
	margin: 2em 0 .5em 0;
    color: #333333;
}	



.footer-list 
{
	list-style: none;
	/*margin-bottom:1em; */
    margin-bottom:0; /*sticky footer bottom gap fix*/
}
.footer-item 
{
	display: inline-block;
	margin: 1em;
	margin-bottom: 0;
}

/*-------- Footer END --------*/

.targetDiv p
{
	text-align: center;
}
.targetDiv {display: none}




.claims_section
{
	margin: 3% 0 0 0;
}
.claims_section .nav-container
{
	padding-bottom: 2%;
}


/* Larger than mobile */
@media (min-width: 400px) {
	
	}

/* Larger than phablet (also point when grid becomes active) */
@media (min-width: 550px) {


	#wrapper {
        min-height:100%;
        position:relative;
    }
    #inner_wrapper {
        padding-bottom:215px; /* Height of the footer element */
    }
    footer
    {
		height:215px;
        position:absolute;
        bottom:0;
        left:0;
    }
	.button-or-solid
    {
    	width:auto;
    }
	section
	{
		margin: 5% 0;
	}
    section.grey
	{
    	margin:0;
		padding: 5% 0;
	}
    section.extra_pad
    {
        padding: 10% 0;
    }

	
	.footer-item
	{
		font-size:.64em;
	}
}

/* Larger than tablet */
@media (min-width: 750px) {
	

    footer .container
    {
    	/*width: 100%;
        max-width: 100%;
        margin: 0;*/
    }
	.footer-item
	{
		font-size:.8em;	
        width:14%;
	}
}

/* Larger than desktop */
@media (min-width: 1000px) {

    .footer-item 
    {
        margin:0;
		font-size:.9em;
    }
}

/* Larger than Desktop HD */
@media (min-width: 1200px)
{

}


</style>